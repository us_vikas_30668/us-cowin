package com.example.us_cowin.ui;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.example.us_cowin.R;
import com.example.us_cowin.data.IPublicApi;
import com.example.us_cowin.data.ServiceBuilder;
import com.example.us_cowin.data.calls.CallForDay;
import com.example.us_cowin.data.models.pin.SessionResponseSchema;
import com.example.us_cowin.data.models.pin.SessionsSchema;
import com.example.us_cowin.databinding.ActivityMainBinding;
import com.example.us_cowin.services.NotifyFGService;

import retrofit2.Call;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements
        CallForDay.ICallForDay {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private ActivityMainBinding mBinding;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        CallForDay callForDay = new CallForDay(this);

        IPublicApi iPublicApi = ServiceBuilder.buildService(IPublicApi.class);
        Call<SessionsSchema> callPin = iPublicApi.findByPin(110001, "31-05-2021");

        mBinding.buttonSearchByDistrict.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, DistrictActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        });

        mBinding.buttonCheckAvailability.setOnClickListener(v -> {
            callForDay.findCallForDay(callPin);
        });

        mBinding.buttonNotify.setOnClickListener(v -> {
//            Intent intent = new Intent(MainActivity.this, NotifyService.class);
            Intent intent = new Intent(MainActivity.this, NotifyFGService.class);
            intent.putExtra("pin", Integer.parseInt(mBinding.etPin.getText().toString()));
//            NotifyService.enqueueWork(this, intent);

            startForegroundService(intent);
        });
    }

    @Override
    public void onSuccess(Response<SessionsSchema> response) {
        if (response.isSuccessful()) {
            SessionsSchema data = response.body();
            for (SessionResponseSchema entry : data.getSessions()) {
                mBinding.textViewResponse.append(entry.toString() + "\n");
            }
        }
        else
            Log.v(LOG_TAG, ">>>> Response not successful: " + response.code());
    }

    @Override
    public void onFailure(Throwable t) {
        Log.v(LOG_TAG, ">>>> Failure: " + t.getMessage());
    }
}