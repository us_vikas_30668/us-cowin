package com.example.us_cowin.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.example.us_cowin.R;
import com.example.us_cowin.data.IPublicApi;
import com.example.us_cowin.data.ServiceBuilder;
import com.example.us_cowin.data.calls.CallForDay;
import com.example.us_cowin.data.models.District;
import com.example.us_cowin.data.models.DistrictsForState;
import com.example.us_cowin.data.models.State;
import com.example.us_cowin.data.models.States;
import com.example.us_cowin.data.models.pin.SessionResponseSchema;
import com.example.us_cowin.data.models.pin.SessionsSchema;
import com.example.us_cowin.databinding.ActivityDistrictBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DistrictActivity extends AppCompatActivity implements
        CallForDay.ICallForDay {

    private static final String LOG_TAG = DistrictActivity.class.getSimpleName();

    private ActivityDistrictBinding mBinding;

    private IPublicApi mIPublicApi;

    private MutableLiveData<HashMap<String, Integer>> mStates = new MutableLiveData<>();
    private MutableLiveData<HashMap<String, Integer>> mDistricts = new MutableLiveData<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_district);

        CallForDay callForDay = new CallForDay(this);
        callGetStates();
        changeButtonStatus(false);

        mBinding.buttonSearchByPin2.setOnClickListener(v -> {
            Intent intent = new Intent(DistrictActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        });

        mStates.observe(this, strings -> {

            List<String> states = new ArrayList<>();
            for(String state : strings.keySet())
                states.add(state);

            ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(DistrictActivity.this, android.R.layout.simple_spinner_item, states);
            mBinding.spinnerState.setAdapter(stateAdapter);

            mBinding.spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String val = states.get(position);
                    callGetDistricts(strings.get(val));
                    changeButtonStatus(false);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });

        mDistricts.observe(this, strings -> {

            List<String> districts = new ArrayList<>();
            for(String district : strings.keySet())
                districts.add(district);

            ArrayAdapter<String> districtAdapter = new ArrayAdapter<>(DistrictActivity.this, android.R.layout.simple_spinner_item, districts);
            mBinding.spinnerDistrict.setAdapter(districtAdapter);
            mBinding.tvDistrictLabel.setVisibility(View.VISIBLE);
            mBinding.spinnerDistrict.setVisibility(View.VISIBLE);
            changeButtonStatus(true);

            mBinding.buttonCheckAvailability2.setOnClickListener(v -> {
                int districtId = strings.get(districts.get(mBinding.spinnerDistrict.getSelectedItemPosition()));
                Call<SessionsSchema> call = mIPublicApi.findByDistrict(districtId, "31-03-2021");
                callForDay.findCallForDay(call);
            });
        });
    }

    private void changeButtonStatus(boolean state) {
        mBinding.buttonCheckAvailability2.setEnabled(state);
        mBinding.buttonNotify2.setEnabled(state);
    }

    private void callGetStates() {

        mIPublicApi = ServiceBuilder.buildService(IPublicApi.class);
        Call<States> call = mIPublicApi.getStates();
        call.enqueue(new Callback<States>() {
            @Override
            public void onResponse(Call<States> call, Response<States> response) {
                if (response.isSuccessful()) {
                    States data = response.body();
                    HashMap<String, Integer> states = new HashMap<>();
                    for (State entry : data.getStates()) {
//                        Log.v(LOG_TAG, "\n" + entry.getStateName());
                        states.put(entry.getStateName(), entry.getStateId());
                    }
                    mStates.setValue(states);
                }
                else
                    Log.v(LOG_TAG, "Waste");
            }

            @Override
            public void onFailure(Call<States> call, Throwable t) {
                Log.v(LOG_TAG, t.getMessage());
            }
        });
    }

    private void callGetDistricts(int districtId) {

        Call<DistrictsForState> call = mIPublicApi.districtsForAState(districtId);
        call.enqueue(new Callback<DistrictsForState>() {
            @Override
            public void onResponse(Call<DistrictsForState> call, Response<DistrictsForState> response) {
                if (response.isSuccessful()) {
                    DistrictsForState data = response.body();
                    HashMap<String, Integer> districts = new HashMap<String, Integer>();
                    for (District entry : data.getDistricts()) {
//                        Log.v(LOG_TAG, "\n" + entry.getDistrictName());
                        districts.put(entry.getDistrictName(), entry.getDistrictId());
                    }
                    mDistricts.setValue(districts);
                    Log.v(LOG_TAG, "Districts fetched");
                }
                else
                    Log.v(LOG_TAG, "Waste");
            }

            @Override
            public void onFailure(Call<DistrictsForState> call, Throwable t) {
                Log.v(LOG_TAG, t.getMessage());
            }
        });
    }

    @Override
    public void onSuccess(Response<SessionsSchema> response) {
        if (response.isSuccessful()) {
            SessionsSchema data = response.body();
            for (SessionResponseSchema entry : data.getSessions()) {
                mBinding.textViewResponse2.append(entry.toString() + "\n");
            }
        }
        else
            Log.v(LOG_TAG, ">>>> Response not successful: " + response.code());
    }

    @Override
    public void onFailure(Throwable t) {
        Log.v(LOG_TAG, ">>>> Failure: " + t.getMessage());
    }
}