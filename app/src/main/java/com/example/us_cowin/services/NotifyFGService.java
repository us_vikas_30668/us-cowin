package com.example.us_cowin.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.us_cowin.ui.MainActivity;
import com.example.us_cowin.R;
import com.example.us_cowin.data.IPublicApi;
import com.example.us_cowin.data.ServiceBuilder;
import com.example.us_cowin.data.calls.CallForDay;
import com.example.us_cowin.data.models.pin.SessionsSchema;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotifyFGService extends Service implements CallForDay.ICallForDay {

    private static final String LOG_TAG = NotifyFGService.class.getSimpleName();

    NotificationManager mNotificationManager;
    String eTag = "";
    CallForDay mCallForDay;

    @Override
    public void onCreate() {
        super.onCreate();
        mCallForDay = new CallForDay(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // create a notification
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification foregroundNotification = myNotificationCreator("Notify You",
                "cowin notification", pendingIntent);

        // start service
        startForeground(1, foregroundNotification);

        // heavy work
        Date date = new Date();
        int pincode = intent.getIntExtra("pin", 0);
        IPublicApi iPublicApi = ServiceBuilder.buildService(IPublicApi.class);
        Call<SessionsSchema> call = iPublicApi.findByPin(pincode, "31-05-2021");

        Handler handler = new Handler(Looper.getMainLooper());

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Log.v(LOG_TAG, "~~~~ date= " + date.toString() + ", pin= " + pincode);
//                mCallForDay.findCallForDay(call);
                findCallByPin(pincode);
                handler.postDelayed(this, 3000);
            }
        };
        handler.post(runnable);

        return START_NOT_STICKY;
    }

    private Notification myNotificationCreator(String title, String description, PendingIntent pendingIntent) {

        Log.v(LOG_TAG, "~~~~ notification build");
        mNotificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "notifyVaccine";
        String channelName = "cowinNotify";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel
                    = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
        }

        Notification notification = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setContentTitle(title)
                .setContentText(description)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .build();

        return notification;
//        int id = 4;
//        mNotificationManager.notify(id, notification);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onSuccess(Response<SessionsSchema> response) {
        if (response.isSuccessful()) {
            SessionsSchema data = response.body();
            String responseETag = response.headers().get("ETag");
            if(eTag.equals(responseETag)){
                Log.v(LOG_TAG, "~~~~ no new vaccine");
            } else {
                if(!TextUtils.isEmpty(eTag)) {
                    eTag = responseETag;
                    Intent notificationIntent = new Intent(this, MainActivity.class);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this,
                            0, notificationIntent, 0);
                    Notification notification = myNotificationCreator("New vaccine in ",
                            "Book new vaccine right away", pendingIntent);
                    int id = 4;
                    mNotificationManager.notify(id, notification);
                }
            }
        }
        else
            Log.v(LOG_TAG, "~~~~ Waste");
    }

    @Override
    public void onFailure(Throwable t) {
        Log.v(LOG_TAG, t.getMessage());
    }

    private void findCallByPin(int pincode) {

        IPublicApi iPublicApi = ServiceBuilder.buildService(IPublicApi.class);
        Call<SessionsSchema> call = iPublicApi.findByPin(pincode, "31-05-2021");

        call.enqueue(new Callback<SessionsSchema>() {
            @Override
            public void onResponse(Call<SessionsSchema> call, Response<SessionsSchema> response) {
                if (response.isSuccessful()) {
                    SessionsSchema data = response.body();
                    String responseETag = response.headers().get("ETag");
                    if(eTag.equals(responseETag)){
                        Log.v(LOG_TAG, "~~~~ no new vaccine");
                    } else {
                        if(!TextUtils.isEmpty(eTag)) {
                            eTag = responseETag;
                            Intent notificationIntent = new Intent(NotifyFGService.this, MainActivity.class);
                            PendingIntent pendingIntent = PendingIntent.getActivity(NotifyFGService.this,
                                    0, notificationIntent, 0);
                            Notification notification = myNotificationCreator("New vaccine in ",
                                    "Book new vaccine right away", pendingIntent);
                            int id = 4;
                            mNotificationManager.notify(id, notification);
                        }
                    }
                }
                else
                    Log.v(LOG_TAG, "~~~~ Waste");
            }

            @Override
            public void onFailure(Call<SessionsSchema> call, Throwable t) {
                Log.v(LOG_TAG, t.getMessage());
            }
        });
    }
}
