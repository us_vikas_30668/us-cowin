package com.example.us_cowin.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;

import com.example.us_cowin.R;
import com.example.us_cowin.data.IPublicApi;
import com.example.us_cowin.data.ServiceBuilder;
import com.example.us_cowin.data.models.pin.SessionsSchema;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotifyService extends JobIntentService {

    private static final String LOG_TAG = NotifyService.class.getSimpleName();
    String eTag = "";

    public static void enqueueWork(Context context, Intent intent){
        enqueueWork(context, NotifyService.class, 11, intent);
    }

    @Override
    protected void onHandleWork(@NonNull @NotNull Intent intent) {

        Date date = new Date();
        int pincode = intent.getIntExtra("pin", 0);

        Handler handler = new Handler(Looper.getMainLooper());

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Log.v(LOG_TAG, "~~~~ date= " + date.toString() + ", pin= " + pincode);
                findCallByPin(pincode);
                handler.postDelayed(this, 3000);
            }
        };
        handler.post(runnable);
    }

    private void findCallByPin(int pincode) {

        IPublicApi iPublicApi = ServiceBuilder.buildService(IPublicApi.class);
        Call<SessionsSchema> call = iPublicApi.findByPin(pincode, "31-05-2021");

        call.enqueue(new Callback<SessionsSchema>() {
            @Override
            public void onResponse(Call<SessionsSchema> call, Response<SessionsSchema> response) {
                if (response.isSuccessful()) {
                    SessionsSchema data = response.body();
                    String responseETag = response.headers().get("ETag");
                    if(eTag.equals(responseETag)){
                        Log.v(LOG_TAG, "~~~~ no new vaccine");
                    } else {
                        if(!TextUtils.isEmpty(eTag)) {
                            eTag = responseETag;
                            myNotificationCreator("New vaccine in " + pincode,
                                    "Book new vaccine right away");
                        }
                    }
                }
                else
                    Log.v(LOG_TAG, "~~~~ Waste");
            }

            @Override
            public void onFailure(Call<SessionsSchema> call, Throwable t) {
                Log.v(LOG_TAG, t.getMessage());
            }
        });
    }

    private void myNotificationCreator(String title, String description) {

        Log.v(LOG_TAG, "~~~~ notification build");
        NotificationManager notificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "notifyVaccine";
        String channelName = "cowinNotify";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel
                    = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        Notification notification = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setContentTitle(title)
                .setContentText(description)
                .setSmallIcon(R.mipmap.ic_launcher)
                .build();

        int id = 4;
        notificationManager.notify(id, notification);
    }
}
