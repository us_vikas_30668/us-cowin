package com.example.us_cowin.data;

import com.example.us_cowin.data.models.CentresByCalendar;
import com.example.us_cowin.data.models.DistrictsForState;
import com.example.us_cowin.data.models.SessionCalendarEntry;
import com.example.us_cowin.data.models.States;
import com.example.us_cowin.data.models.pin.SessionsSchema;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IPublicApi {

    @GET("appointment/sessions/public/calendarByPin")
    Call<CentresByCalendar> calendarByPin(
            @Query("pincode") int pincode,
            @Query("date") String date
    );

    @GET("appointment/sessions/public/findByPin")
    Call<SessionsSchema> findByPin(
            @Query("pincode") int pincode,
            @Query("date") String date
    );

    @GET("appointment/sessions/public/findByDistrict")
    Call<SessionsSchema> findByDistrict(
            @Query("district_id") int districId,
            @Query("date") String date
    );

    @GET("appointment/sessions/public/calendarByDistrict")
    Call<CentresByCalendar> calendarByDistrictId(
            @Query("district_id") int districtId,
            @Query("date") String date
    );

    @Headers({"User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36"})
    @GET("admin/location/districts/{district_id}")
    Call<DistrictsForState> districtsForAState(
            @Path("district_id") int districtId
    );

    @Headers({"User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36"})
    @GET("admin/location/states")
    Call<States> getStates();

}
