package com.example.us_cowin.data.models;

import java.util.List;

public class CentresByCalendar {

    private List<SessionCalendarEntry> centers;

    public List<SessionCalendarEntry> getCenters() {
        return centers;
    }

    public void setCenters(List<SessionCalendarEntry> centers) {
        this.centers = centers;
    }
}
