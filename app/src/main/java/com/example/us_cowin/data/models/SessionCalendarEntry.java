package com.example.us_cowin.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SessionCalendarEntry {

    @SerializedName("center_id")
    private int centerId;

    private String name;

    @SerializedName("name_l")
    private String nameL;

    private String address;

    @SerializedName("address_l")
    private String addressL;

    @SerializedName("state_name")
    private String stateName;

    @SerializedName("state_name_l")
    private String stateNameL;

    @SerializedName("district_name")
    private String districtName;

    @SerializedName("district_name_l")
    private String districtNameL;

    @SerializedName("block_name")
    private String blockName;

    @SerializedName("block_name_l")
    private String blockNameL;

    private String pincode;

    private float lat;

    @SerializedName("long")
    private float lon;

    private String from;

    private String to;

    @SerializedName("fee_type")
    private String feeType;

    @SerializedName("vaccine_fees")
    private List<VaccineFee> vaccineFees;

    private List<Session> sessions;

    public int getCenterId() {
        return centerId;
    }

    public void setCenterId(int centerId) {
        this.centerId = centerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameL() {
        return nameL;
    }

    public void setNameL(String nameL) {
        this.nameL = nameL;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressL() {
        return addressL;
    }

    public void setAddressL(String addressL) {
        this.addressL = addressL;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateNameL() {
        return stateNameL;
    }

    public void setStateNameL(String stateNameL) {
        this.stateNameL = stateNameL;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getDistrictNameL() {
        return districtNameL;
    }

    public void setDistrictNameL(String districtNameL) {
        this.districtNameL = districtNameL;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getBlockNameL() {
        return blockNameL;
    }

    public void setBlockNameL(String blockNameL) {
        this.blockNameL = blockNameL;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public List<VaccineFee> getVaccineFees() {
        return vaccineFees;
    }

    public void setVaccineFees(List<VaccineFee> vaccineFees) {
        this.vaccineFees = vaccineFees;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    @Override
    public String toString() {
        return "SessionCalendarEntry{" +
                "centerId=" + centerId +
                ", name='" + name + '\'' +
                ", nameL='" + nameL + '\'' +
                ", address='" + address + '\'' +
                ", addressL='" + addressL + '\'' +
                ", stateName='" + stateName + '\'' +
                ", stateNameL='" + stateNameL + '\'' +
                ", districtName='" + districtName + '\'' +
                ", districtNameL='" + districtNameL + '\'' +
                ", blockName='" + blockName + '\'' +
                ", blockNameL='" + blockNameL + '\'' +
                ", pincode='" + pincode + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", feeType='" + feeType + '\'' +
                ", vaccineFees=" + vaccineFees +
                ", sessions=" + sessions +
                '}';
    }
}

class VaccineFee {

    private String vaccine;

    private String fee;

    public String getVaccine() {
        return vaccine;
    }

    public void setVaccine(String vaccine) {
        this.vaccine = vaccine;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }
}
