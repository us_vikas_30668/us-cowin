package com.example.us_cowin.data.models;

import com.google.gson.annotations.SerializedName;

public class State {

    @SerializedName("state_id")
    private int stateId;

    @SerializedName("state_name")
    private String stateName;

    @SerializedName("state_name_l")
    private String stateNameL;

    public String getStateNameL() {
        return stateNameL;
    }

    public void setStateNameL(String stateNameL) {
        this.stateNameL = stateNameL;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @Override
    public String toString() {
        return "State{" +
                "stateId=" + stateId +
                ", stateName='" + stateName + '\'' +
                '}';
    }
}
