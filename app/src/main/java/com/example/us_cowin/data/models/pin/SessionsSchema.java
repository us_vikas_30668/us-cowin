package com.example.us_cowin.data.models.pin;

import java.util.List;

public class SessionsSchema {

    private List<SessionResponseSchema> sessions;

    public List<SessionResponseSchema> getSessions() {
        return sessions;
    }
}
