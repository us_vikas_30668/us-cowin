package com.example.us_cowin.data.calls;

import com.example.us_cowin.data.models.pin.SessionsSchema;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallByDistrict {

    private CallByDistrict.ICallByDistrict mICallByDistrict;

    public CallByDistrict(ICallByDistrict iCallByDistrict) {
        mICallByDistrict = iCallByDistrict;
    }

    public void findCallByDistrict(Call<SessionsSchema> call) {

        call.enqueue(new Callback<SessionsSchema>() {
            @Override
            public void onResponse(Call<SessionsSchema> call, Response<SessionsSchema> response) {
                mICallByDistrict.onSuccessDistrict(response);
            }

            @Override
            public void onFailure(Call<SessionsSchema> call, Throwable t) {
                mICallByDistrict.onFailureDistrict(t);
            }
        });
    }

    public interface ICallByDistrict {
        void onSuccessDistrict(Response<SessionsSchema> response);
        void onFailureDistrict(Throwable t);
    }
}
