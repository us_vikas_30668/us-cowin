package com.example.us_cowin.data;

import com.google.gson.GsonBuilder;
import com.google.gson.internal.GsonBuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceBuilder {

    private static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();


    private static OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC))
            .build();

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://cdn-api.co-vin.in/api/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();


    public static <T> T buildService(Class<T> service) {
        return retrofit.create(service);
    }
}
