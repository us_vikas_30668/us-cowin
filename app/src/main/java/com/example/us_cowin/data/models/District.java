package com.example.us_cowin.data.models;

import com.google.gson.annotations.SerializedName;

public class District {

    @SerializedName("state_id")
    private int stateId;

    @SerializedName("district_id")
    private int districtId;

    @SerializedName("district_name")
    private String districtName;

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    @Override
    public String toString() {
        return "District{" +
                "districtId=" + districtId +
                ", districtName='" + districtName + '\'' +
                '}';
    }
}

