package com.example.us_cowin.data.calls;

import com.example.us_cowin.data.models.pin.SessionsSchema;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallForDay {

    private ICallForDay mICallForDay;

    public CallForDay(ICallForDay iCallForDay) {
        mICallForDay = iCallForDay;
    }

    public void findCallForDay(Call<SessionsSchema> call) {

        call.enqueue(new Callback<SessionsSchema>() {
            @Override
            public void onResponse(Call<SessionsSchema> call, Response<SessionsSchema> response) {
                mICallForDay.onSuccess(response);
            }

            @Override
            public void onFailure(Call<SessionsSchema> call, Throwable t) {
                mICallForDay.onFailure(t);
            }
        });
    }

    public interface ICallForDay {
        void onSuccess(Response<SessionsSchema> response);
        void onFailure(Throwable t);
    }
}

