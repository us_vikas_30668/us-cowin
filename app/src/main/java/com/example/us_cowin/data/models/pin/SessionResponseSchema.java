package com.example.us_cowin.data.models.pin;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SessionResponseSchema {

    @SerializedName("center_id")
    private int centerId;

    private String name;

    @SerializedName("name_l")
    private String nameL;

    private String address;

    @SerializedName("address_l")
    private String addressL;

    @SerializedName("state_name")
    private String stateName;

    @SerializedName("state_name_l")
    private String stateNameL;

    @SerializedName("district_name")
    private String districtName;

    @SerializedName("district_name_l")
    private String districtNameL;

    @SerializedName("block_name")
    private String blockName;

    @SerializedName("block_name_l")
    private String blockNameL;

    private String pincode;

    private float lat;

    @SerializedName("long")
    private float lon;

    private String from;

    private String to;

    @SerializedName("fee_type")
    private String feeType;

    private String fee;

    @SerializedName("session_id")
    private String sessionId;

    private String date;

    @SerializedName("available_capacity")
    private int availableCapacity;

    @SerializedName("available_capacity_dose1")
    private int availableCapacityDose1;

    @SerializedName("available_capacity_dose2")
    private int availableCapacityDose2;

    @SerializedName("min_age_limit")
    private int minAgeLimit;

    private String vaccine;

    private List<String> slots;

    public int getCenterId() {
        return centerId;
    }

    public String getName() {
        return name;
    }

    public String getNameL() {
        return nameL;
    }

    public String getAddress() {
        return address;
    }

    public String getAddressL() {
        return addressL;
    }

    public String getStateName() {
        return stateName;
    }

    public String getStateNameL() {
        return stateNameL;
    }

    public String getDistrictName() {
        return districtName;
    }

    public String getDistrictNameL() {
        return districtNameL;
    }

    public String getBlockName() {
        return blockName;
    }

    public String getBlockNameL() {
        return blockNameL;
    }

    public String getPincode() {
        return pincode;
    }

    public float getLat() {
        return lat;
    }

    public float getLon() {
        return lon;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getFeeType() {
        return feeType;
    }

    public String getFee() {
        return fee;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getDate() {
        return date;
    }

    public int getAvailableCapacity() {
        return availableCapacity;
    }

    public int getAvailableCapacityDose1() {
        return availableCapacityDose1;
    }

    public int getAvailableCapacityDose2() {
        return availableCapacityDose2;
    }

    public int getMinAgeLimit() {
        return minAgeLimit;
    }

    public String getVaccine() {
        return vaccine;
    }

    public List<String> getSlots() {
        return slots;
    }

    @Override
    public String toString() {
        return "SessionResponseSchema{" +
                "centerId=" + centerId +
                ", name='" + name + '\'' +
                ", nameL='" + nameL + '\'' +
                ", address='" + address + '\'' +
                ", addressL='" + addressL + '\'' +
                ", stateName='" + stateName + '\'' +
                ", stateNameL='" + stateNameL + '\'' +
                ", districtName='" + districtName + '\'' +
                ", districtNameL='" + districtNameL + '\'' +
                ", blockName='" + blockName + '\'' +
                ", blockNameL='" + blockNameL + '\'' +
                ", pincode='" + pincode + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", feeType='" + feeType + '\'' +
                ", fee='" + fee + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", date='" + date + '\'' +
                ", availableCapacity=" + availableCapacity +
                ", availableCapacityDose1=" + availableCapacityDose1 +
                ", availableCapacityDose2=" + availableCapacityDose2 +
                ", minAgeLimit=" + minAgeLimit +
                ", vaccine='" + vaccine + '\'' +
                ", slots=" + slots +
                '}';
    }
}
